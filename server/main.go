package main

///Libreria GO
import (
	"log"
	"net"
	"net/http"
	"net/rpc"
	"time"
)

// /Api
type API int

// /Funcion RPC Saludar
func (endpoint *API) SaludarConNombre(nombre string, reply *string) error {

	///Mensaje
	log.Println("Se recibe petición del cliente")
	///Se procesa respuesta
	time.Sleep(5 * time.Second)
	respuesta := "Hola " + nombre
	///Se retorna la respuesta
	*reply = respuesta
	///Mensaje
	log.Println("Se envia respuesta")

	///retorna error vacio
	return nil
}

func main() {

	///Instancia API RPC
	var api = new(API)
	err := rpc.Register(api)

	///Validacion Errores
	if err != nil {
		log.Fatal("error registrando API", err)
	}

	///Manipular peticiones HTTP
	rpc.HandleHTTP()

	///Abrir conexion
	listener, err := net.Listen("tcp", ":4040")

	if err != nil {
		log.Fatal("error abriendo conexion", err)
	}

	///Escuchar peticiones
	err = http.Serve(listener, nil)

	if err != nil {
		log.Fatal("error servidor", err)
	}
}
