package main

///Libreria GO
import (
	"fmt"
	"log"
	"net/rpc"
	"time"
)

func main() {

	var reply string

	///Cliente RPC
	client, err := rpc.DialHTTP("tcp", "localhost:4040")
	if err != nil {
		log.Fatal("Error conectandose al server", err)
	}

	///Mensaje
	fmt.Println("Conexión establecida")
	tiempoinicial := time.Now()

	///Establecer conexion RPC
	client.Call("API.SaludarConNombre", "Carlos Julen, Leidy y Andrés ", &reply)

	///Imprimir resultado
	fmt.Println("Respuesta recibida", reply)
	tiempofinal := time.Now()
	diferencia := tiempofinal.Sub(tiempoinicial)
	fmt.Println("Tiempo transcurrido ", diferencia)

}
